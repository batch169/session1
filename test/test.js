const { assert } = require("chai");
const { it } = require("mocha");
const { newUser } = require("../index.js");

describe("Test newUser object", () => {
  it("Assert newUser type is an object", () => {
    assert.equal(typeof newUser, "object");
  });

  it("Assert newUser.email is a string", () => {
    assert.equal(typeof newUser.email, "string");
  });

  it("Assert newUser.email is not undefined", () => {
    assert.notEqual(typeof newUser.email, "undefined");
  });

  it("Assert newUser.password is a string", () => {
    assert.equal(typeof newUser.password, "string");
  });

  it("Assert password is at least 16 characters long", () => {
    assert.isAtLeast(newUser.password.length, 16);
  });
});

describe("Activity test", () => {
  // start of activity
  it("Assert newUser.firstName is a string", () => {
    assert.equal(typeof newUser.firstName, "string");
  });

  it("Assert newUser.lastName is a string", () => {
    assert.equal(typeof newUser.lastName, "string");
  });

  it("Assert newUser.firstName is not undefined", () => {
    assert.notEqual(typeof newUser.firstName, "undefined");
  });

  it("Assert newUser.lastName is not undefined", () => {
    assert.notEqual(typeof newUser.lastName, "undefined");
  });

  it("Assert newUser.age is at least 18", () => {
    assert.isAtLeast(newUser.age, 18);
  });

  it("Assert newUser.age is a number", () => {
    assert.equal(typeof newUser.age, "number");
  });

  it("Assert newUser.contactNo is a string", () => {
    assert.equal(typeof newUser.contactNo, "string");
  });

  it("Assert newUser.batchNo is a number", () => {
    assert.equal(typeof newUser.batchNo, "number");
  });

  it("Assert newUser.batchNo is not undefined", () => {
    assert.notEqual(typeof newUser.batchNo, "undefined");
  });
});
