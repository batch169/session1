const express = require("express");
const app = express();
const port = 3000;

app.use(express.json());

const newUser = {
  firstName: "levi",
  lastName: "ackerman",
  age: 28,
  contactNo: "09123456789",
  batchNo: 166,
  email: "leviackerman@mail.com",
  password: "thequickbrownfoxjumpsoverthelazydog",
};

app.get("/", (req, res) => {
  res.send(newUser);
});

module.exports = {
  newUser: newUser,
};

app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
